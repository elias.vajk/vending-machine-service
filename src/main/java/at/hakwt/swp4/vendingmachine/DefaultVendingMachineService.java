package at.hakwt.swp4.vendingmachine;

import at.hakwt.swp4.vendingmachine.delivery.DeliveryService;
import at.hakwt.swp4.vendingmachine.delivery.InHouseDeliveryService;
import at.hakwt.swp4.vendingmachine.revenue.RevenueDaoImpl;
import at.hakwt.swp4.vendingmachine.revenue.RevenueDao;
import at.hakwt.swp4.vendingmachine.model.Drink;
import at.hakwt.swp4.vendingmachine.model.Order;
import at.hakwt.swp4.vendingmachine.storage.AustrianDrinkStorage;
import at.hakwt.swp4.vendingmachine.storage.DrinkStorageDao;

import java.time.LocalDate;
import java.util.List;

public class DefaultVendingMachineService implements VendingMachineService {

    private final DrinkStorageDao drinkStorage;

    private final DeliveryService deliveryService;

    private final RevenueDao revenueDao;

    public DefaultVendingMachineService(DrinkStorageDao drinkStorage, DeliveryService deliveryService, RevenueDao revenueDao) {
        this.drinkStorage = drinkStorage;
        this.deliveryService = deliveryService;
        this.revenueDao = revenueDao;
    }

    public DrinkStorageDao getDrinkStorage() {
        return drinkStorage;
    }

    public DeliveryService getDeliveryService() {
        return deliveryService;
    }

    public RevenueDao getRevenueDao() {
        return revenueDao;
    }

    /*public DefaultVendingMachineService() {
        this.drinkStorage = new AustrianDrinkStorage();
        this.deliveryService = new InHouseDeliveryService();
        this.revenueDao = new RevenueDaoImpl();
    }*/

    @Override
    public String toString() {
        return "DefaultVendingMachineService{" +
                "drinkStorage=" + drinkStorage +
                ", deliveryService=" + deliveryService +
                ", revenueDao=" + revenueDao +
                '}';
    }

    @Override
    public void drinkSold(String name, Double price) {
        this.revenueDao.registerRevenue(price, LocalDate.now());
    }

    @Override
    public void orderDrinks(Order order) {
        List<Drink> drinks = drinkStorage.getDrinks(order.getDrinkName(), order.getAmount());
        deliveryService.deliver(drinks);
    }

    @Override
    public Double getDailyRevenue() {
        return revenueDao.getDailyRevenue(LocalDate.now());
    }
}
