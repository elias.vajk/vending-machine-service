package at.hakwt.swp4.vendingmachine;

import at.hakwt.swp4.vendingmachine.model.Order;

public interface VendingMachineService {

    void drinkSold(String name, Double price);

    void orderDrinks(Order order);

    Double getDailyRevenue();

}
